public class Deque {

	private final static int ESPAÇO = 2;
	private Object[] arrayI = new Object[ESPAÇO];
	private int inseridos=0;

	public void insereFinal(Object elemento) {
		
		if (inseridos == arrayI.length) {
		
			Object[] arrayE = new Object[inseridos + ESPAÇO];
			
			for (int i = 0; i < arrayI.length; i++) {
				arrayE[i] = arrayI[i];
			}
			
			arrayI = arrayE;
		}
		
		arrayI[inseridos] = elemento;
		inseridos++;
	}
	
	public void insereInicio(Object elemento) {
		if (inseridos == arrayI.length) {
			
			Object[] arrayE = new Object[inseridos + ESPAÇO];
			
			for (int i = 0; i < arrayI.length; i++) {
				arrayE[i] = arrayI[i];
			}
			
			arrayI = arrayE;
		}
		
		arrayI[inseridos] = elemento;
		inseridos++;
	}
		

	public Object removeFim() {
	
		Object obj = arrayI[inseridos];
		
		arrayI[inseridos] = null;
		inseridos--;
		
		return obj;
	}
	
	
	public Object removeInicio() {
		
		Object temp = arrayI[0];
		arrayI[0] = null;
		inseridos--;
		
		for (int i = 0; i < inseridos; i++) {
			arrayI[i] = arrayI[i + 1];
		}
		
		return temp;
	}

	public boolean size() {
		return inseridos == 0;
	}

	public int isEmpty() {
		return inseridos;
	}

	public Object front() {
		return arrayI[0];
	}
	

}
